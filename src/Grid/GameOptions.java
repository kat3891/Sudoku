package Grid;

public class GameOptions{

	int mLevel;
	boolean mCheckGame;
	boolean mKeepPlaying;
	boolean mPause;
	
	public GameOptions(){
		mLevel=1;
		mCheckGame=true;
		mKeepPlaying=true;
		mPause=false;
	}
	public int getLevel(){
		return mLevel;
	}
	
	public boolean getCheckGame(){
		return mCheckGame;
	}
	
	public boolean getContinue(){
		return mKeepPlaying;
	}
	public boolean getPause(){
		return mPause;
	}
	public void initialize(int level, boolean check){
		if (level!=1 && level!=2 &&level!=3) throw new IllegalArgumentException("This level does not exist");
		mLevel=level;		
		mCheckGame=check;
		mKeepPlaying=true;
	}
	
	public void changeCheckGameOption(boolean check){
		mCheckGame=check;
	}
	
	public void stopPlaying(){
		mKeepPlaying=false;
	}
	
	public void changePause(boolean pause){
		mPause=pause;
	}
}
