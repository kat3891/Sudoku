package Grid;
public class RandomIntegerGenerator {

	private int mNumber;
	
	public RandomIntegerGenerator(){
		mNumber=0;
			
	}

	
	public void initialize(int min, int max){
		do{
		mNumber = new Double(Math.random()*30).intValue();
		//Multiplicated by thirty so that sometimes mNumber reach the max
		//Considering that in our case min=0 and max is less than 15
		}
		while (mNumber<min || mNumber>max);
				
	}
	
	public int getInt(){
		return mNumber;
	}

}
