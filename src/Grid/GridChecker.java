package Grid;

public class GridChecker {

	static int SIZE=SudokuGrid.SIZE;
	static double SIZE_OF_A_SCARE=Math.sqrt(SIZE);

	public static boolean isComplete(SudokuGrid sudoku){
		for (int i=0;i<SIZE;i++)
			for (int j=0;j<SIZE;j++){
				if (sudoku.getNumberAtLocation(i,j)==0) return false;
			}
		return true;
	}

	/** Put to wrong all the wrong cases and return true only if the entire grid is correct
	 * 
	 */
	public static void checkGrid(SudokuGrid sudoku){
		boolean [] numbersInRow = new boolean [SIZE];
		boolean [] numbersInColumn = new boolean [SIZE];
		boolean [] numbersInSquare = new boolean [SIZE];
		/* The construction of the lists will be made this way :
		 * in position i, the element is true only if already have seen the number i 
		 *      numbersInRow : in the row that is being checked
		 *      numbersInColum : in the column that is being checked
		 *      numbersInSquare : in the square that is being checked
		 *      
		 *  If we see the number i in a row (or a square or a column), but that we have already seen one before (so the element in the corresponding list is true), the grid is not correct    
		 */

		int number;
		/* Check of rows and columns simultaneously */
		for (int i=0;i<SIZE;i++){
			getAllFalse(numbersInRow);
			getAllFalse(numbersInColumn);
			for (int j=0;j<SIZE;j++){
				//Case where we look the row number i
				number=sudoku.getNumberAtLocation(i,j);
				if (!sudoku.isEmpty(i,j)){
					if (numbersInRow[number-1]) {sudoku.getGrid()[i][j].changeCorrect(false);}
					// The case is false
					else numbersInRow[number-1]=true;
				}
				//Case where we look the column number i
				number=sudoku.getNumberAtLocation(j,i);
				if (!sudoku.isEmpty(j,i)){
					if (numbersInColumn[number-1]) {sudoku.getGrid()[j][i].changeCorrect(false);}
					else numbersInColumn[number-1]=true;
				}
			}
		}

		/* Check of the squares */

		for (int squareRow=0;squareRow<SIZE;squareRow+=SIZE_OF_A_SCARE){
			//Path of the large boxes row by row
			for (int squareColumn=0;squareColumn<SIZE;squareColumn+=SIZE_OF_A_SCARE){
				//Path of the large boxes column by column
				getAllFalse(numbersInSquare); // As we are getting in a new square, we initialize the list to false, f
				for (int i=0;i<SIZE_OF_A_SCARE;i++){
					//The checking of a square is made line by line
					for (int j=0;j<SIZE_OF_A_SCARE;j++){
						//And now column by column
						number=sudoku.getNumberAtLocation(i,j);
						if (!sudoku.isEmpty(i,j)){
							if (numbersInSquare[number-1]) {sudoku.getGrid()[i][j].changeCorrect(false);}
							else numbersInSquare[number-1]=true;
						}
					}
				}
			}
		}
	}

	public static boolean win(SudokuGrid sudoku){
		if (!isComplete(sudoku)) return false;
		
		boolean [] numbersInRow = new boolean [SIZE];
		boolean [] numbersInColumn = new boolean [SIZE];
		boolean [] numbersInSquare = new boolean [SIZE];
		/* The construction of the lists will be made this way :
		 * in position i-1, the element is true only if already have seen the number i 
		 *      numbersInRow : in the row that is being checked
		 *      numbersInColum : in the column that is being checked
		 *      numbersInSquare : in the square that is being checked
		 *      
		 *  If we see the number i in a row (or a square or a column), but that we have already seen one before (so the element in the corresponding list is true), the grid is not correct    
		 */

		int number;
		/* Check of rows and columns simultaneously */
		for (int i=0;i<SIZE;i++){
			getAllFalse(numbersInRow);
			getAllFalse(numbersInColumn);
			for (int j=0;j<SIZE;j++){
				//Case where we look the row number i
				number=sudoku.getNumberAtLocation(i,j);
				if (!sudoku.isEmpty(i,j)){
					if (numbersInRow[number-1]) {return false;}
					else numbersInRow[number-1]=true;
				}
				//Case where we look the column number i
				number=sudoku.getNumberAtLocation(j,i);
				if (!sudoku.isEmpty(i,j)){
					if (numbersInColumn[number-1]) {return false;}
					else numbersInColumn[number-1]=true;
				}
			}
			
		}

		/* Check of the squares */
		getAllFalse(numbersInSquare);
		for (int squareRow=0;squareRow<SIZE;squareRow+=SIZE_OF_A_SCARE){
			//Path of the large boxes row by row
			for (int squareColumn=0;squareColumn<SIZE;squareColumn+=SIZE_OF_A_SCARE){
				
				//Path of the large boxes column by column
				getAllFalse(numbersInSquare); // As we are getting in a new square, we initialize the list to false, f
				for (int i=0;i<SIZE_OF_A_SCARE;i++){
					//The checking of a square is made line by line
					for (int j=0;j<SIZE_OF_A_SCARE;j++){
						//And now column by column
						number=sudoku.getNumberAtLocation(i,j);
						if (!sudoku.isEmpty(i,j)){
							if (numbersInSquare[number-1]) {return false;}
							else numbersInSquare[number-1]=true;
						}
					}
					
				}
			}
		}
		
		// As we never detect any problem, there is no problem
		return true;
	}

	/** So that every component of the list is at false
	 * 
	 * @param list
	 */
	public static void getAllFalse(boolean [] list){
		for (int i=0;i<list.length;i++){
			list[i]=false;
		}
	}
}
