package Grid;

public class Case {

	int mNumber;
	boolean mInitialized;
	boolean mCorrect;
	final public int EMPTY_CASE_NUMBER=0;
	/** Create an empty of a sudoku, so it is not initialized and the number 
	 *  
	 */
	public Case(){
		mNumber=EMPTY_CASE_NUMBER;
		mInitialized=false;
		mCorrect=true;
	}
	
	public void initialize(){
		mNumber=EMPTY_CASE_NUMBER ;
		mInitialized=false;
		mCorrect=true;
	}
	
	public int getNumber(){
		return mNumber;
	}
	
	public boolean isInitialized(){
		return mInitialized;
	}
	
	public void changeCorrect(boolean b){
		mCorrect=b;
	}
	
	public boolean isCorrect(){
		return mCorrect;
	}
	
	public void changeCase(int n, boolean init,boolean cor){
		mNumber=n;
		mInitialized=init;
		mCorrect=cor;
	}
	
	public boolean isEmpty(){
		return mNumber==EMPTY_CASE_NUMBER;
	}
}
