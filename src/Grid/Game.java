package Grid;

public class Game {

	
	static int SIZE=SudokuGrid.SIZE;
	//Static because no instance of the class need to be created to use this function
	public static void addACase(SudokuGrid sudoku,int row,int column, int number){
		if (number<1 ||number>SIZE) throw new IllegalArgumentException("Le chiffre n'est pas dans le bon intervalle");
		if (row<0 || row>SIZE-1) throw new IllegalArgumentException("Vous �tes en dehors de la grille");
		if (column<0 || column>SIZE-1) throw new IllegalArgumentException("Vous �tes en dehors de la grille");
		if (sudoku.isInitializedAtLocation(row,column)) throw new IllegalArgumentException("This has have been given");
		// The case has not been initialized
		else sudoku.changeCase(row,column,number,false,true);
	}	

	public static void eraseCase(SudokuGrid sudoku,int row,int column){
		if (sudoku.isInitializedAtLocation(row, column)) throw new IllegalArgumentException("This case can not be erased");
		else sudoku.eraseCase(row,column);
	}

}
