package Grid;

public class SudokuGrid {

	final public static int SIZE=9;
	private Case [][] mGrid = null;
	private GameOptions mOptions;


	public SudokuGrid(){
		mGrid = new Case [SIZE][SIZE];
		mOptions=new GameOptions();
	}



	public void changeCase(int row,int column, int number, boolean init, boolean corr){
		mGrid[row][column].changeCase(number,init, corr);
	}

	public void eraseCase(int row,int column){
		mGrid[row][column].initialize();
	}

	/** Return the number present in the case (row,column)
	 * 
	 * @param row
	 * @param column
	 * @return
	 */
	public int getNumberAtLocation(int row, int column){
		return mGrid[row][column].getNumber();
	}
	/** To know if the case chosen has been initialized
	 * 
	 * @param row
	 * @param column
	 * @return true if initialized
	 */
	public boolean isInitializedAtLocation(int row, int column){
		return mGrid[row][column].isInitialized();
	}

	public Case [] [] getGrid(){
		return mGrid;
	}
	/** Give the grid's level
	 * 
	 * @return
	 */
	public int getLevel(){
		return mOptions.getLevel();
	}

	/** To know if the player wants to know if there are errors in his grid
	 * 
	 * @return
	 */
	public boolean getCheckGame(){
		return mOptions.getCheckGame();
	}
	/** To know if the player wants to keep playing
	 * 
	 * @return
	 */
	public boolean getContinue(){
		return mOptions.getContinue();
	}

	public boolean getPause(){
		return mOptions.getPause();
	}

	public void initializeToPlay(int level, boolean check){
		mOptions.initialize(level,check);
		for (int i=0;i<SIZE;i++){
			for (int j=0;j<SIZE;j++){ 
				mGrid[i][j]=new Case();
			}
		}
		initializeEmptyGrid();
	
		RandomIntegerGenerator number=new RandomIntegerGenerator();
		number.initialize(1,SIZE);
		
		RandomIntegerGenerator row=new RandomIntegerGenerator();
		row.initialize(0,SIZE-1);
		RandomIntegerGenerator column=new RandomIntegerGenerator();
		column.initialize(0,SIZE-1);
		Game.addACase(this,row.getInt(),column.getInt(),number.getInt());
		Resolveur.resolve(this);
		remove(getRequiredNumberOfInitializedCases(level));
		// For each case which is not empty, the case is correct and has been initialized
		for (int i=0;i<SIZE;i++){
			for (int j=0;j<SIZE;j++){
				if (!isEmpty(i,j)){
					mGrid[i][j].changeCase(getNumberAtLocation(i,j), true, true);
				}
			}
		}

	}

	public int getRequiredNumberOfInitializedCases(int level){
		int numberOfInitializedCases;
		if (level==1){
			numberOfInitializedCases=33;
		}
		else if (level==2){
			numberOfInitializedCases=27;
		}
		else if (level==3){
			numberOfInitializedCases=22;
		} 
		else throw new IllegalArgumentException("This level does not exist");
		return numberOfInitializedCases;
		/** If the number of Initialized Cases can be different for the same level
		 * 		
		 
		RandomIntegerGenerator numberOfInitializedCases=new RandomIntegerGenerator();
		if (level==1) {
			numberOfInitializedCases.initialize(33, 40);
		}
		if (level==2){
			numberOfInitializedCases.initialize(27, 32);
		}
		if (level==3){
			numberOfInitializedCases.initialize(18, 22);
		}
		return numberOfInitializedCases.getInt();
		*/
	}

	public void remove(int numberOfInitializedCases){
		int numberOfCases=SIZE*SIZE;
		int counter=numberOfCases-numberOfInitializedCases;
		RandomIntegerGenerator row=new RandomIntegerGenerator();
		RandomIntegerGenerator column=new RandomIntegerGenerator();
		while (counter>0){
			row.initialize(0,SIZE-1);
			column.initialize(0,SIZE-1);
			if (!isEmpty(row.getInt(),column.getInt())){
			Game.eraseCase(this,row.getInt(),column.getInt());
			counter--;	
			}
		}
	}

	public void initializeEmptyGrid(){
		mOptions.initialize(3, false);
		for (int i=0;i<SIZE;i++){
			for (int j=0;j<SIZE;j++){ 
				mGrid[i][j]=new Case();
			}
		}
	}

	public boolean isEmpty(int row,int column){
		return mGrid[row][column].isEmpty();
	}

	public void initializeCase(int row,int column){
		mGrid[row][column].initialize();
	}


	public void changePause(boolean pause) {
		mOptions.changePause(pause);
	}

	public void stopPlaying() {
		mOptions.stopPlaying();	
	}





}
