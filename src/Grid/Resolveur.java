package Grid;

public class Resolveur {

	public final static int SIZE=SudokuGrid.SIZE;
	public static double SQUARE_SIZE=Math.sqrt(SIZE);



	/** The function resolves the grid given.
	 * We admit that the grid given has a solution
	 * @param sudoku
	 */
	public static void resolve(SudokuGrid sudoku){
		int row=0;
		int column=0;
		boolean bool=isCorrect(sudoku,row,column);
		if (!bool) throw new IllegalArgumentException("This Grid has no Solution");
	}

	/** The grid that has been given has only been initialized.
	 * The fonction completes the grid by following the Sudoku's rules.
	 * 
	 * @return true if the grid has been correctly fulfilled.
	 */
	public static boolean isCorrect (SudokuGrid sudoku,int rowPosition, int columnPosition){
	
		//if we get out of the grid, then everything is OK
		if (rowPosition==SIZE && columnPosition==0){
			return true;
		}

		//if the case is not empty
		if (!sudoku.isEmpty(rowPosition, columnPosition)){
			if (columnPosition<SIZE-1){
				return isCorrect(sudoku,rowPosition,columnPosition+1);
			}
			else {
				return isCorrect(sudoku,rowPosition+1,0);
			}
		}


		/* We use the backtracking method to fulfill the grid. We fulfill the cases one by one
		 if we finally have no solutions because of the number we chose,
		 */
		else {
			
			for (int possibleNumber=1;possibleNumber<=SIZE;possibleNumber++){
				if (missingInRow(sudoku,rowPosition,possibleNumber) && missingInColumn(sudoku,columnPosition,possibleNumber) && missingInSquare(sudoku,rowPosition,columnPosition,possibleNumber)){
					sudoku.changeCase(rowPosition, columnPosition, possibleNumber, false, true);

					/* The principle is easy, we call isCorrect() on the next case
					 * if the function return true, it means that by writing possibleNumber in the current case, we still have a grid that can be completed by following Sudoku's rules.
					 * if not, it means that we can not finished the grid and so it means that the choice we made is wrong
					 * Indeed, we suppose that the given grid can be finished with at least one possibility
					 */
					if (columnPosition<SIZE-1){
						if (isCorrect(sudoku,rowPosition,columnPosition+1)){
							return true;
						}
					}
					else {
						if (isCorrect(sudoku,rowPosition+1,0)) return true;
					};
					sudoku.initializeCase(rowPosition,columnPosition);
				}
				/*if none of the numbers can be written, it means that there is a mistake somewhere
		so we erase the first case we change : position (rowPostion,columnPosition)
		and we return false because the grid is not correct */
			}
			return false;
		}
	}


	public static boolean missingInRow(SudokuGrid sudoku,int row, int number){
		for (int j=0; j <SIZE; j++){
			if (sudoku.getNumberAtLocation(row,j)==number){
				return false;
			}
		}
		return true;
	}

	public static boolean missingInColumn(SudokuGrid sudoku,int column, int number){
		for (int i=0;i<SIZE; i++){
			if (sudoku.getNumberAtLocation(i,column)==number){
				return false;	
			}
		}
		return true;
	}

	public static boolean missingInSquare(SudokuGrid sudoku,int row, int column, int number)	{
		int FirstRow=row-(row%3);
		int FirstColumn=column-(column%3);
		for (int i=FirstRow;i-FirstRow<SQUARE_SIZE;i++){
			for(int j=FirstColumn;j-FirstColumn<SQUARE_SIZE;j++){
				if (number==sudoku.getNumberAtLocation(i, j)) {return false;}
			}
		}
		return true;
	}

}



