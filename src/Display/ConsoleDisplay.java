package Display;
import java.io.IOException;
import java.util.Scanner;

import Grid.Game;
import Grid.GridChecker;
import Grid.Resolveur;
import Grid.SudokuGrid;
public class ConsoleDisplay extends GridDisplay {

	final int SIZE=SudokuGrid.SIZE;
	int level;
	int row;
	int column;
	int number;
	int action;
	int pause;
	boolean checkGame;
	Scanner userInput = new Scanner(System.in);

	public ConsoleDisplay(SudokuGrid sudoku){
		super(sudoku);
	}

	/** PRINCIPAL MENU */
	
	/**Display the Principal Menu where the user choose if he wants to play a game, resolve a Sudoku Grid or leave the application
	 * 
	 * @throws IOException  */

	public void displayPrincipalMenu() throws IOException{
		System.out.println("(1) Play");
		System.out.println("(2) Resolve a sudoku");
		System.out.println("(3) Quit");
		int optionChosen;
		do{
			optionChosen=userInput.nextInt();
		}
		while (optionChosen!=1 && optionChosen!=2 && optionChosen!=3);
		if (optionChosen==1){
			displayChoseLevel();
			displayChoseCheckGameOption();
			initializeToPlay(level,checkGame);
			displaySudokuGame();
		}
		else if (optionChosen==2){
			System.out.println("********* Resolver ************* ");
			initializeEmptyGrid();
			askGrid();
			resolveGrid();
			displayGrid();
		}
		else {};
	}
/** COMMON METHODS */
	/** Displays the grid but never shows the wrong cases
	 * 
	 */
	public void displayGrid(){
		System.out.println("-------------------------------");
		for (int i=0;i<SIZE;i++){
			System.out.print('|');
			for (int j=0;j<SIZE;j++){
				System.out.print(" "+mSudoku.getGrid()[i][j].getNumber()+" ");
				if (j%3==2 && j!=8) System.out.print('|');
			}
			System.out.println('|');
			if (i%3==2) System.out.println("-------------------------------");
			
			}
		}
	
	public void displayTurn(){
		System.out.println("(1) Add a number");
		System.out.println("(2) Erase a number");
		do{
			action=userInput.nextInt();
		}
		while (action!=1 && action!=2);
		System.out.println("Which row ?");
		row=userInput.nextInt();
		System.out.println("Which column?");
		column=userInput.nextInt();
		if (action==1){
		System.out.println("Which number ?");
		number=userInput.nextInt();
		}
	}
 
 /** SUDOKU GAME */
	public void displaySudokuGame() throws IOException{
		System.out.println("********* Game *********");
		displayGrid();
		while (!GridChecker.win(mSudoku) && mSudoku.getContinue()){
			displayTurn();
			if (action==1){
				Game.addACase(mSudoku,row,column,number);
				}
			else Game.eraseCase(mSudoku,row,column);
			if (mSudoku.getCheckGame()){
				GridChecker.checkGrid(mSudoku);
			}
			System.out.println("Pause?");
			System.out.println("(1) Yes");
			System.out.println("(2) No");
			do{
			pause=userInput.nextInt();
			}
			while (pause!=1 && pause!=2);
			mSudoku.changePause(pause==1);
			if (mSudoku.getPause()){
				displayMenuWhilePlaying();
			}
			displayGrid();
		}
		if (GridChecker.win(mSudoku)) {
			displayYouWin();
		}
		displayPrincipalMenu();
	}

	
	

	
	public void displayChoseLevel(){
		System.out.println("Game Level");
		System.out.println("(1) Easy");
		System.out.println("(2) Medium");
		System.out.println("(3) Expert");
		level=userInput.nextInt();

	}

	public void displayChoseCheckGameOption(){
		System.out.println("Do you want to check the game at each turn ?");
		System.out.println("(1) Yes ");
		System.out.println("(2) No");
		checkGame=(userInput.nextInt()%2==1);
	}

	

	public void displayYouWin(){
		System.out.println("You win");
	}

	/** Display the menu when the user is playing and what to leave the game or make a pause
	 * 
	 * @throws IOException
	 */
	public void displayMenuWhilePlaying() throws IOException{
		System.out.println("(1) Continue");
		System.out.println("(2) Quit");
		int optionChosen;
		do {
			optionChosen=userInput.nextInt();}
		while (optionChosen!=1 && optionChosen!=2);
		if (optionChosen==2) {
			mSudoku.stopPlaying();
			displayPrincipalMenu();
		}
		else {
			mSudoku.changePause(false);
			displaySudokuGame();		
		}
	}

/** SUDOKU RESOLVEUR */
	
	public void askGrid(){
		boolean isComplete=false;
		int complete;
		displayGrid();
		do{
			displayTurn();
			if (action==1){
				Game.addACase(mSudoku,row,column,number);
				}
			else Game.eraseCase(mSudoku,row,column);
			displayGrid();
			System.out.println("Is the grid complete ?");
			System.out.println("(1) Yes");
			System.out.println("(2) No");
			do {
				complete=userInput.nextInt();	
			}
			while (complete!=1 && complete!=2);
			isComplete=(complete==1);
		}
		while(!isComplete);
	}
	public void resolveGrid(){
		Resolveur.resolve(mSudoku);
	}


}




