package Display;

import java.io.IOException;
import java.util.Scanner;

import Grid.SudokuGrid;
public class WindowDisplay extends GridDisplay {

	final int SIZE=SudokuGrid.SIZE;
	int level;
	int row;
	int column;
	int number;
	boolean checkGame;
	Scanner userInput = new Scanner(System.in);
	
	public WindowDisplay(SudokuGrid grid) {
		super(grid);
	}
	
	public void initialize(int number, boolean check) throws IOException{
		initialize(number, check);
	}

	public void displaySudokuGame() throws IOException{
			
	}

	public void displayGrid(){
	
	}

	public void displayChoseLevel(){


	}

	public void displayChoseCheckGameOption(){
	
	}

	public void displayTurn(){
		
	}

	public void displayYouWin(){
		
	}

	public void displayMenuWhilePlaying() throws IOException{
				
	}
	

	public void displayPrincipalMenu(){
		
	}

}
