package Display;

import java.io.IOException;

import Grid.SudokuGrid;

public class GameLaunch {

	
	public static void main(String [] args) throws IOException{
		SudokuGrid grid = new SudokuGrid();
		ConsoleDisplay cgrid=new ConsoleDisplay(grid);
		cgrid.displayPrincipalMenu();
	}
}
