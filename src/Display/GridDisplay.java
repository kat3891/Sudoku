package Display;

import java.io.IOException;

import Grid.SudokuGrid;


public abstract class GridDisplay {

	protected SudokuGrid mSudoku;
	
	public GridDisplay(SudokuGrid sudoku){
		mSudoku=sudoku;
	}
	
	public void initializeToPlay(int level, boolean check) throws IOException {
		mSudoku.initializeToPlay(level,check);
	}

	public void initializeEmptyGrid(){
		mSudoku.initializeEmptyGrid();
	}
	
}


