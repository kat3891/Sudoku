# Sudoku

## Game Rules#
In a grid of 9x9 cases, each row, column and square 3x3 must contains each number between 1 and 9, but only once.

## Two Options for the User #
1. Play 
2. Resolve a grid

### Option 1 : The user resolves the grid. #


For now, the creation of the random grid takes about 10minutes : about 40 numbers out of 81 are chosen randomly. Then the resolver resolves the grid (if there is a solution, otherwise the grid is erased before being build randomly once more) and a precise number of cases are erased, depending on the level chosen by the user : EASY, MEDIUM, EXPERT.
The algorithm checks during this step that the grid has still one and only one solution.


### Option 2 : The user wants to get a solution for the grid he gave. #


The user gives his/her grid. Then, the algorithm resolves it, if the grid has at least one solution and respects the rules.
The resolver is based on the backtracking principle : the algorithm completes one by one the cases with numbers according to Sudoku Rules. If at a moment it ends in an gridlock, the algorithm goes back.

*NB : If the given grid has more than one solution, the algorithm will give one possible solution.*
